package com.property.trends.model;

import static com.property.trends.model.PropertyDetails.PropertyDetailsFields.*;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PropertyDetails {

    public static class PropertyDetailsFields {
        public static final String PROPERTY_REFERENCE = "propertyReference";
        public static final String PRICE = "price";
        public static final String BEDROOMS = "bedrooms";
        public static final String BATHROOMS = "bathrooms";
        public static final String HOUSE_NUMBER = "houseNumber";
        public static final String ADDRESS = "address";
        public static final String REGION = "region";
        public static final String POSTCODE = "postcode";
        public static final String PROPERTY_TYPE = "propertyType";
    }

    @JsonProperty(value = PROPERTY_REFERENCE, required = true)
    private long propertyReference;
    @JsonProperty(value = PRICE, required = true)
    private long price;
    @JsonProperty(value = BEDROOMS, required = true)
    private int bedrooms;
    @JsonProperty(value = BATHROOMS, required = true)
    private int bathrooms;
    @JsonProperty(value = HOUSE_NUMBER, required = true)
    private String houseNumber;
    @JsonProperty(value = ADDRESS, required = true)
    private String address;
    @JsonProperty(value = REGION, required = true)
    private String region;
    @JsonProperty(value = POSTCODE, required = true)
    private String postcode;
    @JsonProperty(value = PROPERTY_TYPE, required = true)
    private String propertyType;
}
