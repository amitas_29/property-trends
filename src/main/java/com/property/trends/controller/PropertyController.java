package com.property.trends.controller;

import java.util.List;
import java.util.OptionalDouble;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.property.trends.model.PropertyDetails;
import com.property.trends.service.PropertyTrendsProcessor;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@Api(value = "", produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE, description = "Property price trends.")
public class PropertyController {

    @Autowired
    PropertyTrendsProcessor propertyTrendsProcessor;

    @ApiOperation(value = "Find mean price for postcode outward. Enter letters of the required postcode e.g.WIF")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Found the mean price for given postcode!"),
            @ApiResponse(code = 500, message = "Mean price calculation for postcode failed"),
            @ApiResponse(code = 404, message = "Hint: Please check url follows format /priceByPostCode/{postcode}")
    })
    @RequestMapping(value = "/priceByPostCode/{postCode}",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON,
            produces = MediaType.TEXT_PLAIN)

    public String findMeanPriceForPostcode(@ApiParam(value = "Property  details in JSON format as POST body", required = true) @RequestBody PropertyDetails[] propertyDetails,
                                           @ApiParam(value = "Postcode appended in the request url ", required = true) @PathVariable("postCode") String postCode) throws Exception {
        log.debug("Postcode for finding mean price  " + postCode);
        OptionalDouble averagePrice = propertyTrendsProcessor.findMeanPriceForPostcode(propertyDetails, postCode);
        return averagePrice.isPresent() ? Double.valueOf(averagePrice.getAsDouble()).toString()
                : "Either there is no property for this postcode or there is issue calculating mean price for this postcode";
    }

    @ApiOperation(value = "Find mean difference in price for given 2 different property types")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Found the mean price difference between entered property types"),
            @ApiResponse(code = 500, message = "Price difference calculation failed"),
            @ApiResponse(code = 404, message = "Hint: Please check url follows format /priceByPropertyType/{propertyTypeA}/{propertyTypeB}")

    })
    @RequestMapping(value = "/priceByPropertyType/{propertyTypeA}/{propertyTypeB}",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON,
            produces = MediaType.TEXT_PLAIN)

    public String findAverageDifferenceInPropertyTypes(@ApiParam(value = "Property  details in JSON format as POST body", required = true) @RequestBody PropertyDetails[] propertyDetails,
                                                       @ApiParam(value = "Exact property type name A", required = true) @PathVariable("propertyTypeA") String propertyTypeA,
                                                       @ApiParam(value = "Exact property type name B", required = true) @PathVariable("propertyTypeB") String propertyTypeB) throws Exception {
        log.debug("Property Type A ::  " + propertyTypeA);
        log.debug("Property Type B ::  " + propertyTypeB);
        Double averagePrice = propertyTrendsProcessor.findAverageDifferenceInPropertyTypes(propertyDetails, propertyTypeA, propertyTypeB);
        return averagePrice > 0.0 ? averagePrice.toString() : "Either there is no difference in price of given property types or property types not found";
    }

    @ApiOperation(value = "Find top expensive properties")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Found top x percent properties by price"),
            @ApiResponse(code = 500, message = "Top property price calculation failed"),
            @ApiResponse(code = 404, message = "Hint: Please check url follows format /topPrice/{cutOffLimit}")
    })
    @RequestMapping(value = "/topPrice/{cutOffLimit}",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON,
            produces = MediaType.APPLICATION_JSON)

    public List<PropertyDetails> findTopExpensiveProperties(@ApiParam(value = "Property  details in JSON format as POST body", required = true) @RequestBody PropertyDetails[] propertyDetails,
                                                            @ApiParam(value = "Desired cutoff percentage appended in the request url ", required = true) @PathVariable("cutOffLimit") String cutOffLimit) throws Exception {
        log.debug("Cut off Limit ::  " + cutOffLimit);
        return propertyTrendsProcessor.findTopExpensiveProperties(propertyDetails, cutOffLimit);
    }
}
