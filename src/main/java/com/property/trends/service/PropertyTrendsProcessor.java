package com.property.trends.service;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.property.trends.model.PropertyDetails;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PropertyTrendsProcessor {

    // Find the mean price in the postcode outward ‘W1F’?
    public OptionalDouble findMeanPriceForPostcode(PropertyDetails[] propertyDetails, String postCode) {

        OptionalDouble averagePriceForPostcode = Arrays.stream(propertyDetails)
                .filter(propertyDetail -> postCode.equals(propertyDetail.getPostcode().split("\\s+")[0]))
                .mapToLong(propertyDetail -> propertyDetail.getPrice())
                .average();
        log.debug(String.format("Average price for property for postcode %s is %s", postCode, averagePriceForPostcode));
        return averagePriceForPostcode;
    }

    // Find the difference in average property prices between detached houses and flats?
    public Double findAverageDifferenceInPropertyTypes(PropertyDetails[] propertyDetails, String propertyTypeA, String propertyTypeB) {
        OptionalDouble averagePriceA = Arrays.stream(propertyDetails)
                .filter(propertyDetail -> propertyTypeA.equals(propertyDetail.getPropertyType()))
                .mapToLong(propertyDetail -> propertyDetail.getPrice())
                .average();

        if(averagePriceA.isPresent()) {
            log.debug(String.format("Average price for property type %s  is %s", propertyTypeA, averagePriceA.getAsDouble()));
        }

        OptionalDouble averagePriceB = Arrays.stream(propertyDetails)
                .filter(propertyDetail -> propertyTypeB.equals(propertyDetail.getPropertyType()))
                .mapToLong(propertyDetail -> propertyDetail.getPrice())
                .average();
        if(averagePriceB.isPresent()){
            log.debug(String.format("Average price for property type %s  is %s", propertyTypeB, averagePriceB.getAsDouble()));
        }
        return averagePriceA.isPresent() && averagePriceB.isPresent() ? averagePriceA.getAsDouble() - averagePriceB.getAsDouble() : 0.0;

    }

    //Find the top 10% most expensive properties
    public List<PropertyDetails> findTopExpensiveProperties(PropertyDetails[] propertyDetails, String cutOffPercentage) throws NumberFormatException {
        int cutOffPercentageIntValue = 0;
        try {
            cutOffPercentageIntValue = Integer.parseInt(cutOffPercentage);
            log.debug("Valid cut off percentage " +cutOffPercentage);
        }catch(NumberFormatException e) {
            throw new NumberFormatException("Invalid cut off percentage, make sure to enter Integer value");
        }
        int limit = (propertyDetails.length * cutOffPercentageIntValue) / 100;
        log.debug(String.format("Number of records %d", limit));
        return Arrays.stream(propertyDetails)
                .sorted(Comparator.comparing(PropertyDetails::getPrice).reversed())
                .limit(limit)
                .collect(Collectors.toList());
    }
}
