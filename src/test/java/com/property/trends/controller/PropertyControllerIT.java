package com.property.trends.controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;

import javax.annotation.Resource;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.property.trends.model.PropertyDetails;
import com.property.trends.TestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@EnableWebMvc
@ComponentScan(basePackages = {
        "com.property.trends.*"
})
public class PropertyControllerIT {

    @Resource
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    private final static ObjectMapper mapper = new ObjectMapper();

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    @Test
    public void shouldReturnSuccessOnValidRequest() throws Exception {
        mockMvc.perform(post("/priceByPostCode/AB1")
                .contentType(APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(TestUtils.getPropertyDetailsWithPostCode())))
                .andExpect(status().isOk())
                .andExpect(status().is(200));
    }

    @Test
    public void shouldReturnCorrectAveragePriceForPostCode() throws Exception {
        String expectedMeanPriceForPostcode = "3000.0";
        MvcResult result = mockMvc.perform(post("/priceByPostCode/AB1")
                .contentType(APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(TestUtils.getPropertyDetailsWithPostCode())))
                .andExpect(status().isOk())
                .andReturn();
        String actualMeanPriceForPostCode = result.getResponse().getContentAsString();
        assertThat(actualMeanPriceForPostCode, equalTo(expectedMeanPriceForPostcode));
    }

    @Test
    public void shouldReturnCorrectAveragePriceForPropertyTypes() throws Exception {
        String expectedDifferenceInPropertyPrice = "800.0";
        MvcResult result = mockMvc.perform(post("/priceByPropertyType/Mansion/Flat")
                .contentType(APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(TestUtils.getPropertyDetailsWithPropertyTypes())))
                .andExpect(status().isOk())
                .andReturn();
        String actualDifferenceInPropertyPrice = result.getResponse().getContentAsString();
        assertThat(Double.valueOf(actualDifferenceInPropertyPrice).toString(), equalTo(expectedDifferenceInPropertyPrice));
    }

    @Test
    public void shouldReturnCorrectCorrectTopProperties() throws Exception {

        PropertyDetails[] expectedTop50PercentProperties = { TestUtils.makePropertyDetails(1000L),
                TestUtils.makePropertyDetails(700l),
                TestUtils.makePropertyDetails(600l),
                TestUtils.makePropertyDetails(500l),
                TestUtils.makePropertyDetails(400l)
        };

        PropertyDetails[] propertyDetails = {
                TestUtils.makePropertyDetails(100l),
                TestUtils.makePropertyDetails(200l),
                TestUtils.makePropertyDetails(1000l),
                TestUtils.makePropertyDetails(500l),
                TestUtils.makePropertyDetails(400l),
                TestUtils.makePropertyDetails(600l),
                TestUtils.makePropertyDetails(350l),
                TestUtils.makePropertyDetails(200l),
                TestUtils.makePropertyDetails(300l),
                TestUtils.makePropertyDetails(700l)};

        MvcResult result = mockMvc.perform(post("/topPrice/50")
                .contentType(APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(propertyDetails)))
                .andExpect(status().isOk())
                .andReturn();
        String actualTop50PercentPropertiesString = result.getResponse().getContentAsString();
        PropertyDetails[] actualTop50PercentProperties = mapper.readValue(actualTop50PercentPropertiesString, PropertyDetails[].class);
        assertThat(actualTop50PercentProperties, equalTo(expectedTop50PercentProperties));
    }

    @Test
    public void whenRequestContentTypeMismatch_thenUnsupportedMediaType() throws Exception {
        mockMvc.perform(post("/priceByPostCode/AB1")
                .contentType(MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isUnsupportedMediaType())
                .andExpect(status().is(415));
    }

    @Test
    public void whenHttpRequestMethodNotSupported_thenNotFound() throws Exception {
        mockMvc.perform(post("/random"))
                .andExpect(status().isNotFound())
                .andExpect(status().is(404));
    }

    @Test
    public void whenHttpRequestMethodMismatch_thenMethodNotAllowed() throws Exception {
        mockMvc.perform(get("/priceByPostCode/AB1"))
                .andExpect(status().isMethodNotAllowed())
                .andExpect(status().is(405));
    }

}
