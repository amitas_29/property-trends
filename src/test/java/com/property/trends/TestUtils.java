package com.property.trends;

import com.property.trends.model.PropertyDetails;

public class TestUtils {

    public static PropertyDetails makePropertyDetails(String postCode, long price, long propertyReference) {
        return new PropertyDetails()
                .setAddress("Sample Adress 1")
                .setBathrooms(3)
                .setBedrooms(3)
                .setHouseNumber("1a")
                .setPostcode(postCode)
                .setPrice(price)
                .setPropertyReference(propertyReference)
                .setPropertyType("Flat")
                .setRegion("AB");
    }

    public static PropertyDetails makePropertyDetails(long propertyReference, String propertyType, long price) {
        return new PropertyDetails()
                .setAddress("Sample Adress 1")
                .setBathrooms(3)
                .setBedrooms(3)
                .setHouseNumber("1a")
                .setPostcode("AB0 1CD")
                .setPrice(price)
                .setPropertyReference(propertyReference)
                .setPropertyType(propertyType)
                .setRegion("AB");
    }

    public static PropertyDetails makePropertyDetails(long price) {
        return new PropertyDetails()
                .setAddress("Sample Adress 1")
                .setBathrooms(3)
                .setBedrooms(3)
                .setHouseNumber("1a")
                .setPostcode("AB0 1CD")
                .setPrice(price)
                .setPropertyReference(1)
                .setPropertyType("Flat")
                .setRegion("AB");
    }

    public static PropertyDetails[] getPropertyDetailsWithPostCode() {
        return new PropertyDetails[]{
                TestUtils.makePropertyDetails("AB1 0CD", 3000L, 1),
                TestUtils.makePropertyDetails("AB1 0FG", 2000L, 2),
                TestUtils.makePropertyDetails("HI1 0LM", 3000L, 3),
                TestUtils.makePropertyDetails("AB1 0NN", 4000L, 4),
                TestUtils.makePropertyDetails("JK1 0PQ", 5000L, 5)
        };
    }

    public static PropertyDetails[] getPropertyDetailsWithPropertyTypes() {
        return new PropertyDetails[]{
                TestUtils.makePropertyDetails(1, "Flat", 100L),
                TestUtils.makePropertyDetails(2, "Mansion", 1000L),
                TestUtils.makePropertyDetails(3, "Detached", 1000L),
                TestUtils.makePropertyDetails(4, "Flat", 300L),
                TestUtils.makePropertyDetails(5, "Mansion", 1000L),
                TestUtils.makePropertyDetails(6, "Mansion", 1000L),
                TestUtils.makePropertyDetails(7, "Flat", 200L)
        };
    }
}
