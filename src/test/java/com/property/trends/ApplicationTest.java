package com.property.trends;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@EnableWebMvc
@ComponentScan(basePackages = {
        "com.property.trends.controller",
        "com.property.trends.model",
        "com.property.trends.service"
})
public class ApplicationTest extends AbstractJUnit4SpringContextTests {
    @Test
    public void contextLoads() {}
}

