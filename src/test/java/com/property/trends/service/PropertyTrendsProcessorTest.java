package com.property.trends.service;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.property.trends.model.PropertyDetails;
import com.property.trends.TestUtils;

public class PropertyTrendsProcessorTest {

    PropertyTrendsProcessor propertyTrendsProcessor = new PropertyTrendsProcessor();

    @Test
    public void shouldReturnCorrectMeanPriceForGivenPostcode() throws Exception {
        double expectedMeanPriceForPostcode = 3000;
        double actualMeanPriceForPostcode = propertyTrendsProcessor.findMeanPriceForPostcode(TestUtils.getPropertyDetailsWithPostCode(), "AB1").getAsDouble();
        assertEquals(expectedMeanPriceForPostcode, actualMeanPriceForPostcode, 0.0);
    }

    @Test
    public void shouldReadThePostcodeSplitBySpace() throws Exception {
        double expectedMeanPriceForPostcode = 3000;
        PropertyDetails[] propertyDetails = {
                TestUtils.makePropertyDetails("AB10 0CD", 3000L, 1),
                TestUtils.makePropertyDetails("AB10 0FG", 2000L, 2),
                TestUtils.makePropertyDetails("HI1 0LM", 3000L, 3),
                TestUtils.makePropertyDetails("AB10 0NN", 4000L, 4),
                TestUtils.makePropertyDetails("JK1 0PQ", 5000L, 5)
        };
        double actualMeanPriceForPostcode = propertyTrendsProcessor.findMeanPriceForPostcode(propertyDetails, "AB10").getAsDouble();
        assertEquals(expectedMeanPriceForPostcode, actualMeanPriceForPostcode, 0.0);

    }

    @Test
    public void shouldReturnCorrectAverageDifferenceInPropertyType() throws Exception {
        double averagePriceForFlat = 200;
        double averagePriceForMansion = 1000;
        double expectedDifferenceInPropertyPrice = averagePriceForMansion - averagePriceForFlat;
        double actualDifferenceInPropertyPrice = propertyTrendsProcessor.findAverageDifferenceInPropertyTypes(TestUtils.getPropertyDetailsWithPropertyTypes(), "Mansion", "Flat");
        assertEquals(expectedDifferenceInPropertyPrice, actualDifferenceInPropertyPrice, 0.0);
    }

    @Test
    public void shouldReturnCorrectOrderOfTopExpensiveProperties() throws Exception {

        List<PropertyDetails> expectedTop10PercentProperties = Arrays.asList(TestUtils.makePropertyDetails(1000L));
        List<PropertyDetails> expectedTop20PercentProperties = Arrays.asList(TestUtils.makePropertyDetails(1000L),
                TestUtils.makePropertyDetails(700l));
        List<PropertyDetails> expectedTop30PercentProperties = Arrays.asList(TestUtils.makePropertyDetails(1000L),
                TestUtils.makePropertyDetails(700l),
                TestUtils.makePropertyDetails(600l));
        List<PropertyDetails> expectedTop40PercentProperties = Arrays.asList(TestUtils.makePropertyDetails(1000L),
                TestUtils.makePropertyDetails(700l),
                TestUtils.makePropertyDetails(600l),
                TestUtils.makePropertyDetails(500l));
        List<PropertyDetails> expectedTop50PercentProperties = Arrays.asList(TestUtils.makePropertyDetails(1000L),
                TestUtils.makePropertyDetails(700l),
                TestUtils.makePropertyDetails(600l),
                TestUtils.makePropertyDetails(500l),
                TestUtils.makePropertyDetails(400l));

        PropertyDetails[] propertyDetails = {
                TestUtils.makePropertyDetails(100l),
                TestUtils.makePropertyDetails(200l),
                TestUtils.makePropertyDetails(1000l),
                TestUtils.makePropertyDetails(500l),
                TestUtils.makePropertyDetails(400l),
                TestUtils.makePropertyDetails(600l),
                TestUtils.makePropertyDetails(350l),
                TestUtils.makePropertyDetails(200l),
                TestUtils.makePropertyDetails(300l),
                TestUtils.makePropertyDetails(700l)};

        List<PropertyDetails> actualTop10PercentExpensiveProperties = propertyTrendsProcessor.findTopExpensiveProperties(propertyDetails, "10");
        List<PropertyDetails> actualTop20PercentExpensiveProperties = propertyTrendsProcessor.findTopExpensiveProperties(propertyDetails, "20");
        List<PropertyDetails> actualTop30PercentExpensiveProperties = propertyTrendsProcessor.findTopExpensiveProperties(propertyDetails, "30");
        List<PropertyDetails> actualTop40PercentExpensiveProperties = propertyTrendsProcessor.findTopExpensiveProperties(propertyDetails, "40");
        List<PropertyDetails> actualTop50PercentExpensiveProperties = propertyTrendsProcessor.findTopExpensiveProperties(propertyDetails, "50");

        assertEquals(expectedTop10PercentProperties, actualTop10PercentExpensiveProperties);
        assertEquals(expectedTop20PercentProperties, actualTop20PercentExpensiveProperties);
        assertEquals(expectedTop30PercentProperties, actualTop30PercentExpensiveProperties);
        assertEquals(expectedTop40PercentProperties, actualTop40PercentExpensiveProperties);
        assertEquals(expectedTop50PercentProperties, actualTop50PercentExpensiveProperties);

    }

    @Test(expected=NumberFormatException.class)
    public void shouldThrowErrorIfPercentageIsNotAValidNumber() throws Exception {
        PropertyDetails[] propertyDetails = {
                TestUtils.makePropertyDetails(100l),
                TestUtils.makePropertyDetails(700l)};
        List<PropertyDetails> actualTop10PercentExpensiveProperties = propertyTrendsProcessor.findTopExpensiveProperties(propertyDetails, "xx");
    }
}
